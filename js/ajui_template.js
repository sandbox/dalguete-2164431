/**
 * Autocomplete jQuery UI template file, used to load js modifier per field
 */

(function(Drupal){
  
  // Set the correct value for the correct selector into Drupal.ajui
  Drupal.ajui[SELECTOR_REPLACE] = CODE_REPLACE;
  
})(Drupal);