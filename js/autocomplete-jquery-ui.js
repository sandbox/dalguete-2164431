/**
 * Autocomplete jQuery UI widget
 */
(function($){
  
  // Initializes the ajui container inside Drupal object
  Drupal.ajui = Drupal.ajui || {};
  
  /**
   * Behavior to apply the AutocompleteJQueryUI into defined elements
   */
  Drupal.behaviors.AutocompleteJQueryUI = {
    attach: function (context, settings) {
      
      // Basic object used to apply some alterations in the autocomplete jQuery UI process
      // Defined here as there needs to be just one version of it, and not defined
      // to prevent turn it changeable
      var ajui = {
        
        modify: function(options){
          // Default implementation that does a basic transformation
          var optionsAux = [];
          
          $.each(options, function(key, value){
            optionsAux[optionsAux.length] = { label: value, value: key };
          });
          
          return optionsAux;
        },

        change: function($element, e, ui){
          // Default implementation that does nothing
        },
                
        search: function($element, e, ui){
          // Default implementation that does nothing
        },
        
        select: function($element, e, ui){
          // Default implementation that does nothing
        }
      };        
      
      // Helper function used to call a function from the ajui object
      var callFunction = function(selector, funcName){
        // Get the selector posted by the server, used as base for the ajui
        // file returned
        var selectorId = $(selector).data('selector');

        // Get the ajui object passed (if any)
        var oAjui = Drupal.ajui[selectorId] || ajui;

        // Validates the function, before calling it
        var func = oAjui[funcName] || ajui[funcName];
        
        if(typeof func === 'function'){
          var args = Array.prototype.slice.call(arguments, 2);
          
          return func.apply(null, args);
        }
      };
      
      // Initialize the jQuery UI Autocomplete to every found element
      $('.form-autocomplete-jquery-ui').once('autocomplete-jquery-ui').each(function(index) {
        // Create the cache
        var cache = {};
        
        // Get the form build idelement id
        var formBuildId = $(this).parents('form').find('input[name=form_build_id]').val();
        
        // Get the element id
        var id = $(this).attr('id');
        
        // Extract info about the autocomplete from the helpers
        var $mainPath = $(this).siblings('#' + id + '-main-path-info');
        var $elementInfo = $(this).siblings('#' + id + '-element-info');
        
        // Reattach every Drupal.ajui entry with the correct element. Specially
        // useful when the form has been altered via ajax processes, and all the
        // ids are new (of course)
        for (var selector in Drupal.ajui){
          $(selector).data('selector', selector);
        }
        
        // Set the autocomplete
        $(this).autocomplete({
          
          // Activate the html support
          html: true,
          
          // Set the data source
          source: function(request, response) {
            // Term to request
            var term = request.term;
            
            // Search the term in cache. If found, returns it
            if (term in cache) {
              response(cache[term]);

              return;
            }

            // Get the form build id, and add it to the request
            request['form_build_id'] = formBuildId;
            
            // Get the element info that triggered the request
            request['element-info'] = $elementInfo.val();
            
            // Check to see if the ajui object must be reloaded
            if(!$('#' + id).data('selector') || !Drupal.ajui[$('#' + id).data('selector')]){
              request['ajui-load'] = true;
            }

            // Generate an ajax post request for the options
            $.post($mainPath.val(), request, function(data, status, xhr) {
              // Create a dumb Drupal.ajax object
              var drupalAjax = new Drupal.ajax(null, $(document), {url: '/', event: 'dumb event'});

              // Perform a success operation on the dumb Drupal.ajax object
              drupalAjax.success(data, status);
              
              // Get the list of options set as data in the element
              var options = $('#' + id).data('options');
              
              // TODO: to be fixed in the future to use the event response. Check TODO below
              // Call the correct function to work with (if available)
              options = callFunction('#' + id, 'modify', options);
              
              // Cache the data
              cache[term] = options;
              
              // Send the response
              response(options);              
            });
          },
                  
          // TODO: This is the correct way of doing things, but because of the
          // jQuery UI version, this is not working yet. To be fixed in the future
          // http://stackoverflow.com/questions/12788804/jquery-autocomplete-response-event
//          // Set the action to perform on select
//          response: function(event, ui) {
//            options = callFunction('#' + id, 'responder', $(this), event, ui);
//          },
                  
          // Set the action to perform on change
          change: function(event, ui) {
            // Call the correct function to work with (if available)
            callFunction('#' + id, 'change', $(this), event, ui);
          },
                  
          // Set the action to perform on search
          search: function(event, ui) {
            // Call the correct function to work with (if available)
            callFunction('#' + id, 'search', $(this), event, ui);
          },
                  
          // Set the action to perform on select
          select: function(event, ui) {
            // Call the correct function to work with (if available)
            callFunction('#' + id, 'select', $(this), event, ui);
          }
        });
      });
    }
  };

})(jQuery);
